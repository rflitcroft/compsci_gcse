import time

# The clock function works differently on Windows than it does on Unix
# Therefore I have used my own, so the behaviour is consistent
class Time:
    def __init__(self):
        self.__startTime = 0
        self.__started = False

    def clock(self):
        if (self.__started):
            return time.time() - self.__startTime

        else:
            self.__started = True
            self.__startTime = time.time()
            return 0

# def clock():
#     if clock.isStarted:
#         return time.time() - clock.startTime
#     else:
#         clock.startTime = time.time()
#         clock.isStarted = True
#         return 0

timer = Time()

# Returns the length of the String passed in
def LEN(myString) -> int:
    return len(myString)


# Outputs some values to the console, separated by a space
def OUTPUT(*values):
    print(*values, sep = '')


# Outputs some values to the console
def USERINPUT() -> str:
    return input()


##################### Main ######################

sentence = "The quick brown fox jumps over the lazy dog"
n = LEN(sentence)
OUTPUT("Sentence to type: " + sentence)
errorFlag = False
OUTPUT("Press Enter when you're ready to start typing! Press Enter when finished")
ready = USERINPUT()
OUTPUT("Go!")

timer.clock() # starts the clock

mySentence = USERINPUT()

finishTime = timer.clock()

if (mySentence != sentence):
    errorFlag = True

totalTime = round(finishTime, 1)
if errorFlag == True:
    OUTPUT("You made one or more errors")
else:
    OUTPUT("Total time taken ", totalTime, " seconds")
    OUTPUT("Your sentence contained ", n, " characters")
