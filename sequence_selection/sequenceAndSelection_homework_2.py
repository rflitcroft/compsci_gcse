import random


# Returns the length of the String passed in
def LEN(myString) -> int:
    return len(myString)


# Outputs some values to the console
def OUTPUT(*values):
    print(*values, sep = ' ')


# Inputs a single character from the console
def USERINPUT() -> chr:
    return input()


def RANDOM_INT(lower, upper) -> int:
    return random.randrange(lower, upper + 1)


def POSITION(char, string) -> int:
    return string.find(char)


def CHAR_AT(pos, string) -> chr:
    return string[pos]


##################### Main ######################

# password checker
customerPassword = "123456789" #(assume read from file)
numChars = LEN(customerPassword)
index1 = RANDOM_INT(0, numChars - 1)
index2 = RANDOM_INT(0, numChars - 1)
index3 = RANDOM_INT(0, numChars - 1)
OUTPUT("Please enter characters", index1 + 1, index2 + 1, index3 + 1, "from your password")
OUTPUT("Press enter after each character:")
char1 = USERINPUT()
char2 = USERINPUT()
char3 = USERINPUT()

if (POSITION(char1, customerPassword) == index1
    and POSITION(char2, customerPassword) == index2
    and POSITION(char3, customerPassword) == index3):
# if (CHAR_AT(index1, customerPassword) == char188
#     and CHAR_AT(index2, customerPassword) == char2
#     and CHAR_AT(index3, customerPassword) == char3):


    OUTPUT("Welcome")
else:
    OUTPUT("Password incorrect")