import math

PI = math.pi

diameter = float(input("What is the diameter of the cylinder?"))
height = float(input("What is the height of the cylinder?"))

circumference = PI * diameter
radius = diameter/2
areaOfTop = PI * radius * radius

drumArea = (circumference * height) + (2 * areaOfTop)

coverageAreaPerPaintpot = float(input("What is the max area of the desired paint pot?"))

numberOfPaintpotsNeeded = math.ceil(drumArea/coverageAreaPerPaintpot)

print("You need ", numberOfPaintpotsNeeded, "pots to paint the drum.")
