# Returns user input as a CHAR type
def USERINPUT():
    return input()


# Outputs some values to the console
def OUTPUT(*values):
    print(*values, sep = '')


userName = ["Carl","Tamsin","Eric","Zoe","Alan","Mark"]
numItems = len(userName)

for i in range(0, numItems - 1):
    for j in range(0, numItems - i - 1):
        if userName [j] > userName [j+1]:
            lower = userName[j]
            higher = userName[j+1]

            # Swap the names in the array
            userName[j+1] = lower
            userName[j] = higher

            OUTPUT("Progress: ", userName)

OUTPUT("Sorted: ", userName)
