names = ["Anna", "Bill",    "David", "Faisal", "Jasmine", "Jumal",
         "Ken",  "Michela", "Pavel", "Rosa",   "Stepan",  "Tom",  "Zac"]

itemSought = input("Please enter name to search for: ")
itemFound = False
searchFailed = False
lastIndex = len(names) - 1
firstIndex = 0
midpoint = 0

while ((not itemFound) and (not searchFailed)):
    midpoint = (firstIndex + lastIndex) // 2
    print("firstIndex, lastIndex, midpoint, name", firstIndex, lastIndex, midpoint, names[midpoint])

    if names[midpoint] == itemSought:
        itemFound = True
    else:
        if firstIndex > lastIndex:
            searchFailed = True
        else:
            if names[midpoint] < itemSought:
                firstIndex = midpoint + 1
            else:
                lastIndex = midpoint - 1

if itemFound:
    print("item is at index ", midpoint)
else:
    print ("item is not in the array")


# There is an error in the pseudo-code; midpoint is not defined !

'''
A = ["Anna","Bill","David","Faisal","Jasmine","Jumal",
     "Ken","Michela","Pavel","Rosa","Stepan","Tom","Zac"]
OUTPUT ("Please enter name to search for: ")
itemSought  USERINPUT
itemFound  False
searchFailed  False
lastIndex  len(A) – 1
firstIndex  0

WHILE (not itemFound) AND (not searchFailed)
    	midpoint  int((firstIndex + lastIndex)/2)
    	OUTPUT (“firstIndex, lastIndex, midpoint, name”, firstIndex, 
					lastIndex, midpoint, A[midpoint])
   	IF A[midpoint] = itemSought THEN
       	itemFound  True
    	ELSE
        	IF firstIndex > lastIndex THEN
            		searchFailed  True
        	ELSE
            		IF A[midpoint] < itemSought THEN
               		 firstIndex  midpoint + 1      
            		ELSE
                		lastIndex  midpoint – 1
			ENDIF
		ENDIF
	ENDIF
ENDWHILE
IF itemFound THEN
   	 print(“item is at position “,midpoint)
ELSE
    	print (“item is not in the array”)
ENDIF
'''