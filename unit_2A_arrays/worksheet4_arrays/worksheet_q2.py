# Returns user input as a CHAR type
def USERINPUT():
    return input()


# Outputs some values to the console
def OUTPUT(*values):
    print(*values, sep = '')


months = ["January", "February", "March", "April", "May", "June", "July",
          "August", "September", "October", "November", "December"]


rainfall = [0.0] * len(months)

# Capture all the rainfall data
for i in range(0, len(months)):
    OUTPUT("Enter rainfall for ", months[i])
    rainfall[i] = float(USERINPUT())

# Calculate total rainfall you the year
totalRainfall = 0.0
for i in range(0, len(months)):
    totalRainfall = totalRainfall + rainfall[i]

OUTPUT("Annual rainfall (to 1 d.p.: ", round(totalRainfall, 1))

# Calcuate monthly average
average = totalRainfall / len(months)
OUTPUT("Monthly average rainfall: ", average)


# Calculate number of months with higher than average rainfall
count = 0
for i in range(0, len(months)):
    if (rainfall[i] > average):
        count = count + 1

OUTPUT("Number of months with higher than average rainfall: ", count)
