# Returns user input as a CHAR type
def USERINPUT():
    return input()


# Outputs some values to the console
def OUTPUT(*values):
    print(*values, sep = '')


months = ["January", "February", "March", "April", "May", "June", "July",
          "August", "September", "October", "November", "December"]


temps = [0.0] * len(months)

for i in range(0, len(months)):
    OUTPUT("Enter temperature for ", months[i])
    temps[i] = float(USERINPUT())


OUTPUT("Month".ljust(15, ' '), "| Temp.")
OUTPUT("---------------+-------------")
for i in range(0, len(months)):
    OUTPUT(months[i].ljust(15, ' '), "| ", str(temps[i]).rjust(4))
