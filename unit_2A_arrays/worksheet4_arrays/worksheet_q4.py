# Returns user input as a CHAR type
def USERINPUT():
    return input()


# Outputs some values to the console
def OUTPUT(*values):
    print(*values, sep = '')

# Input names
name = [""] * 3
for i in range(0, len(name)):
    OUTPUT("Enter name ", i + 1, ":")
    name[i] = USERINPUT()


marks = [[0 for x in range(5)] for y in range(3)]
# Input marks
for i in range(0, len(name)):
    OUTPUT("Student: ", name[i])

    for j in range(0, 5):
        OUTPUT("Mark ", j + 1, ":")
        marks[i][j] = int(USERINPUT())


for i in range(0, len(name)):
    total = 0
    myMarks = marks[i]

    # Total mark per student
    for j in range(0, len(myMarks)):
        total = total + myMarks[j]

    # Average mark per student
    average = total / len(myMarks)

    OUTPUT("Student ", name[i], " - Total: ", total, " Average: ", average)

