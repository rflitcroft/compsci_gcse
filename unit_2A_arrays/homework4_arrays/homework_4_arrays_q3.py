def USERINPUT():
    return int(input())

    # Outputs some values to the console


def OUTPUT(*values):
    print(*values, sep='')


sales = [
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0]
]

totalsales = [0, 0, 0, 0, 0]

for product in range(0, 5):
    OUTPUT("Sales for product ", product + 1)

    for month in range(0, 3):
        OUTPUT("Enter quantity for month ", month + 1, ":")
        sales[month][product] = USERINPUT()

        #complete statement here
        totalsales[product] = totalsales[product] + sales[month][product]

# insert code here to print total sales for each product
for product in range(0, 5):
    OUTPUT("Total sales for product ", product + 1, " = ", totalsales[product])

