def USERINPUT():
    return input()

    # Outputs some values to the console


def OUTPUT(*values):
    print(*values, sep=' ')


ID = [45, 33, 27, 88, 103, 66, 71]
OUTPUT("Please enter ID number to find")
numberSought = int(USERINPUT())
found = False
n = len (ID)
k = 0

while found == False and (k < n):
    if numberSought == ID [k]:
        found = True
    k = k + 1

if found == True:
    OUTPUT ("ID is in the list at index", k - 1)
else:
    OUTPUT ("ID is not in the list")