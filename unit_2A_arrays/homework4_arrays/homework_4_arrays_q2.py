def USERINPUT():
    return input()

    # Outputs some values to the console


def OUTPUT(*values):
    print(*values, sep=' ')


monthName = ["January", "February", "March", "April", "May", "June", "July",
          "August", "September", "October", "November", "December"]

billAmounts = [None] * len(monthName)

for m in range(0, len(monthName)):
    OUTPUT("Enter bill for ", monthName[m])
    billAmounts[m] = float(USERINPUT())

highestBillMonth = 0
for m in range(0, len(monthName)):
    if billAmounts[m] > billAmounts[highestBillMonth]:
        highestBillMonth = m

OUTPUT("Highest bill was", monthName[highestBillMonth], ":", billAmounts[highestBillMonth])