# Outputs some values to the console
def OUTPUT(*values):
    print(*values, sep = ', ')


def CHAR_TO_CODE(char) -> int:
    return ord(char)


def CODE_TO_CHAR(code) -> chr:
    return chr(code)


################ Main ################

x = CHAR_TO_CODE('b')

y = x + 1

z = x - 1

w = CODE_TO_CHAR(y) + CODE_TO_CHAR(z)

OUTPUT(x, y, z, w)
