# Returns input from console converted to an integer
def USERINPUT() -> int:
    return int(input())


# Outputs some values to the console
def OUTPUT(*values):
    print(*values, sep = ', ')


OUTPUT("Please enter values for a, b, and c")

a = USERINPUT()
b = USERINPUT()
c = USERINPUT()

x = a * b + c
y = (b + c) / a
z = x // b
w = x % b
x = x + y

OUTPUT(x, y, z, w)


