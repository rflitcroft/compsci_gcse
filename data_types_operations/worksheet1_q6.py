# Outputs some values to the console
def OUTPUT(*values):
    print(*values, sep = ', ')


# Returns user input as a CHAR type
def USERINPUT() -> chr:
    return input()


# Converts a char to its ASCII code
def CHAR_TO_CODE(char) -> int:
    return ord(char)


# Converts an ASCII code to its char
def CODE_TO_CHAR(code):
    return chr(code)


########## Main ################

OUTPUT("Please enter character: ")
plaintext = USERINPUT()

codedASCII = CHAR_TO_CODE(plaintext) + 3

if codedASCII >= 122:
    codedASCII = codedASCII - 26

codedChar = CODE_TO_CHAR(codedASCII)
OUTPUT(codedChar)