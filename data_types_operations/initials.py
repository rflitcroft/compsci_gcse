# Returns input from console converted to an integer
def USERINPUT():
    return input()


def OUTPUT(*values):
    print(*values, sep='')


def POSITION(char, string):
    return string.find(char)


def SUBSTRING(start, end, string):
    return string[start:end + 1]


def LEN(input):
    return len(input)


OUTPUT("Enter you full name:")
fullName = USERINPUT()
spaceIndex = POSITION(' ', fullName)
firstNameInitial = SUBSTRING(0, 0, fullName)
lastName = SUBSTRING(spaceIndex, LEN(fullName) - 1, fullName)
OUTPUT(firstNameInitial, lastName)

