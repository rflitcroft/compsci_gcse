# Outputs some values to the console
def OUTPUT(*values):
    print(*values, end = "|\n", sep='')


def SUBSTRING(start, end, string) -> str:
    return string[start:end + 1]


def POSITION(string, char) -> int:
    return string.find(char)


########## Main ###############

notice = "Please do not walk on the grass"
start = POSITION(notice, " ")
end = POSITION(notice, "t")
x = SUBSTRING(0, start, notice)
stringLength = len(notice)
y = SUBSTRING(end + 2, stringLength - 1, notice)
OUTPUT("x = ", x)
OUTPUT("y = ", y)
OUTPUT(x + y)